FROM python:3.6

EXPOSE 5000

WORKDIR /app

RUN apt-get update && \
    apt-get install -y sudo && \
    rm -rf /var/lib/apt/lists/* && \
    adduser --disabled-password --gecos '' --uid 1001 docker && \
    gpasswd -a docker sudo && \
    echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers && \
    chown -R docker:docker /app

USER docker

COPY --chown=docker ./feedback /app
RUN pip install -r requirements.txt

CMD ["python", "app.py"]
