FROM python:3.7-buster

EXPOSE 5000

WORKDIR /app

RUN rm -rf /var/lib/apt/lists/* && \
    adduser --disabled-password --gecos '' --uid 1001 docker && \
    gpasswd -a docker sudo && \
    echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers && \
    chown -R docker:docker /app

USER docker

COPY --chown=docker ./event /app

RUN pip install -r requirements.txt

ENTRYPOINT ["python","run.py"]
CMD [""]
